class AddBlogTextEasyUrlToIdea < ActiveRecord::Migration
  def change
    add_column :ideas, :blog, :text
    add_column :ideas, :slug, :string
    rename_column :ideas, :meta_description, :description

    add_index :ideas, :slug, :unique => true
  end
end
