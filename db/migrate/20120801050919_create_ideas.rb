class CreateIdeas < ActiveRecord::Migration
  def change
    create_table :ideas do |t|
      t.string :title
      t.text :meta_description
      t.string :seller_name
      t.decimal :price
      t.string :affiliate_url
      t.boolean :active
      t.integer :counter_cache

      t.timestamps
    end
  end
end
