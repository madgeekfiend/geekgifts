class CreateAffiliateLinks < ActiveRecord::Migration
  def change
    create_table :affiliate_links do |t|
      t.string :company
      t.text :html
      t.integer :idea_id

      t.timestamps
    end
  end
end
