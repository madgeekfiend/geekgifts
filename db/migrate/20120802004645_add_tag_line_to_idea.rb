class AddTagLineToIdea < ActiveRecord::Migration
  def change
    add_column :ideas, :tag_line, :string
  end
end
