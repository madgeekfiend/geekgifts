class AddImageColumnsToIdea < ActiveRecord::Migration
  def change
    add_column :ideas, :small_image, :string
    add_column :ideas, :large_image, :string
  end
end
