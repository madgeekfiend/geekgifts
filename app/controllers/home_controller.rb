class HomeController < ApplicationController

  def index
    @ideas = Idea.where( :active=>true ).shuffle
    @title = "Total Geek Stuff::Cool Gift Ideas and stuff for Geeks"
    @meta_description = "Cool gift ideas for that special geek in your life. Or, cool stuff for geeks to buy themselves! Add these geek items to your collection. Get ideas on the latest geek items."
    @meta_keywords = "Gifts, Geeks, nerds, nerd, geek, techie, cool gift, cool, gifts for geeks, geek gifts, geek gift"
  end

end
