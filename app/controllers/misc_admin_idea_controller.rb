class MiscAdminIdeaController < ApplicationController
  before_filter :authenticate_admin!

  def add_tag_idea
    @idea = Idea.find( params[:id] )
    @tags = @idea.tags.map { |t| t.name }
    #@tags = []
    #@idea.tags.select { |t| @tags << t.name }
  end

  def post_tag_idea
    if ( params[:tags].present? && params[:id].present? )
      # Add this to the tags thing
      idea = Idea.find(params[:id])
      idea.tag_list = params[:tags]
      idea.save
      render :text => "Added tags: #{ params[:tags]} to Idea with title: #{ idea.title}", :layout => nil and return
    end

    render :text => "Couldn't save oh-oh"
  end

end
