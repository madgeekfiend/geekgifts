class TagController < ApplicationController

  def gifts_for
    #display all the gifts with a particular tag
    @ideas = Idea.tagged_with( params[:tag], :any=>true )
    @title = "Total Geek Stuff | Gift Ideas and stuff for #{params[:tag]}"
    @meta_description = "Gift Ideas for the geek in your life. These are gift ideas for #{params[:tag]}. Visit the home page for more gift ideas."
    @meta_keywords = "geek stuff,geek gifts,gifts for #{params[:tag]},geek,geek items"
    render :template => 'home/index'
  end

end
