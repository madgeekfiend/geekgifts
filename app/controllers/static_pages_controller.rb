class StaticPagesController < ApplicationController
caches_page :about

  def about
    @title = "Total Geek Stuff::About Us - Cool Gift Ideas and stuff for Geeks"
    @meta_description = "Learn more about Total Geek Stuff and why this website was created. Meet a very geeky dude."
    @meta_keywords = "about us, description, sam contapay, sam, geek, about a geek"
  end
end
