class IdeasController < ApplicationController

  def slug
    @idea = Idea.find_by_slug( params[:slug] )
    @title = "Total Geek Stuff | Gift for Geeks | #{@idea.title}"
    @meta_description = "Get #{@idea.title} a cool geek gift for that special geek in your life or just buy it yourself."
  end
end
